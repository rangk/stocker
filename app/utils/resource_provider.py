import os
import platform


if 'darwin' in platform.system().lower():
    RESOURCE_DIR = "./resources"
else:
    RESOURCE_DIR = ".\\resources"


class ResourceProvider:
    def __init__(self, context=None):
        """
        Context is program
        :param context:
        """
        self._resource_dir = RESOURCE_DIR
        self._ui_dir = os.path.join(self._resource_dir, "ui")

    def get_ui_path(self, name):
        ui_file_path = os.path.join(self._ui_dir, "{}.ui".format(name))
        if os.path.exists(ui_file_path):
            return ui_file_path

        return None
