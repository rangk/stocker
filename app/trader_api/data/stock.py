from typing import Optional
from dataclasses import dataclass, field


@dataclass(frozen=True)
class Stock:
    name: str
    code: str
    current_value: int

    def __str__(self):
        return "{} | {} | {}".format(self.code, self.name, self.current_value)

    def __eq__(self, other):
        assert isinstance(other, Stock), "Stock can compare with only Stock Data, but it is {}".format(other)
        return self.code == other.code


# @dataclass(frozen=True)
# class Transaction:
#     stock_name: str = field(compare=False)
#     stock_code: str = field(compare=True)
#     at_time: str = field(compare=True)
#     flag: int = field(compare=False)
#     current_price: int = field(compare=False)
#     diff: int = field(compare=False)
#     volume: int = field(compare=False)
#     total_volume: int = field(compare=False)


@dataclass(frozen=True)
class CreonStock(Stock):
    strategy_id: str = field(default=None)
    monitor_id: str = field(default=None)
    catch_at: str = field(default=None)


# class StockData:
#     def __init__(self,
#                  name,
#                  stock_id,
#                  strategy_id: Optional,
#                  monitor_id: Optional,
#                  current_value: Optional,
#                  catched_at: Optional):
#         self._name = name
#         self._stock_id = stock_id
#         self._strategy_id = strategy_id
#         self._monitor_id = monitor_id
#         self._current_value = current_value
#         self._catched_at = catched_at
#
#     @property
#     def name(self):
#         return self._name
#
#     @property
#     def stock_id(self):
#         return self._stock_id
#
#     @property
#     def strategy_id(self):
#         return self._strategy_id
#
#     @property
#     def monitor_id(self):
#         return self._monitor_id
#
#     @property
#     def current_value(self):
#         return self._current_value
#
#     @property
#     def catched_at(self):
#         return self._catched_at
#
#
# class CreonStockData(StockData):
#     def __str__(self):
#         return "S_ID {} | {}".format(self._strategy_id, self._name)
