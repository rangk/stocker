from typing import Optional, Union
import abc


class StrategyData(abc.ABC):

    @abc.abstractmethod
    def serialize(self):
        pass


class CreonStrategyData(StrategyData):
    """
    Must implement __str__
    """

    def __init__(self, name: str, s_id: Union[str, int], created_at: str,
                 owner: Optional[str] = None, avg_stocks: Optional[str] = None,
                 avg_income: Optional[str] = None, avg_win_rate: Optional[str] = None):
        self._name = name
        self._s_id = s_id
        self._created_at = created_at
        self._owner = owner
        self._avg_stocks = avg_stocks
        self._avg_income = avg_income
        self._avg_win_rate = avg_win_rate

    @property
    def name(self):
        return self._name

    @property
    def created_at(self):
        return self._created_at

    @property
    def strategy_id(self):
        return self._s_id

    @property
    def strategy_name(self):
        return self._name

    def serialize(self):
        """
        TODO
        :return:
        """
        return ""

    def __str__(self):
        return "{}".format(self._name)
