import abc
from abc import ABC
from typing import List, Literal, Dict, Optional
from dataclasses import dataclass, field, fields


@dataclass(frozen=True)
class Strategy:
    strategy_id: str = field(compare=True)
    monitor_id: str = field(compare=False, default=None)
    name: str = field(compare=False, default=None)
    created_at: str = field(compare=False, default=None)
    owner: str = field(compare=False, default=None)
    avg_stocks: int = field(compare=False, default=None)
    avg_income: float = field(compare=False, default=None)
    avg_win_rate: float = field(compare=False, default=None)

    def __str__(self):
        return "{} | {}".format(self.strategy_id, self.name)


@dataclass(frozen=True)
class WatchStrategy:
    strategy_id: str = field(compare=True)
    name: str = field(compare=False)
    monitor_id: str = field(compare=False, default=None)

    def update_monitor_id(self, monitor_id: str):
        assert int(monitor_id) > 0, "monitor id Must bigger than 0"
        return WatchStrategy(self.strategy_id, self.name, monitor_id)


@dataclass(frozen=True)
class Stock:
    name: str = field(compare=True)
    code: str = field(compare=True)
    current_value: int = field(compare=False, default=0)

    def __str__(self):
        return "{} | {} | {}".format(self.code, self.name, self.current_value)

    def __eq__(self, other):
        assert isinstance(other, Stock), "Stock can compare with only Stock Data, but it is {}".format(other)
        return self.code == other.code


@dataclass(frozen=True)
class WatchStock:
    name: str = field(compare=True)
    code: str = field(compare=True)


@dataclass(frozen=True)
class Transaction:
    stock_name: str = field(compare=False)
    stock_code: str = field(compare=True)
    at_time: str = field(compare=True)
    flag: int = field(compare=False)
    current_price: int = field(compare=False)
    diff: int = field(compare=False)
    volume: int = field(compare=False)
    total_volume: int = field(compare=False)


def create_stock_from_dict(data_dict: Dict) -> Stock:
    return Stock(**data_dict)


def create_transaction_from_dict(data_dict: Dict) -> Transaction:
    return Transaction(**data_dict)


@dataclass(frozen=True)
class SellStock:
    """
    TODO : Implement
    """
    name: str = field(compare=True)
    code: str = field(compare=True)
    mode: str
    to_price: int
    limit_loss_percent: int


@dataclass(frozen=True)
class BuyStock:
    """
    TODO : Implement
    """
    name: str = field(compare=True)
    code: str = field(compare=True)
    mode: str
    to_price: int


class IMarketResponse(abc.ABC):
    @abc.abstractmethod
    def on_watched_strategy_stock(self, stock: Stock, strategy: Strategy):
        pass

    @abc.abstractmethod
    def on_watched_stock_price(self, transaction: Transaction):
        pass


class StrategyApi(abc.ABC):
    @abc.abstractmethod
    def get_strategies(self, is_test: bool) -> List[Strategy]:
        pass

    @abc.abstractmethod
    def find_strategy_monitor_id(self, target: WatchStrategy) -> Optional[str]:
        pass

    @abc.abstractmethod
    def watch_strategy(self, target: WatchStrategy) -> Optional[List[Stock]]:
        pass

    @abc.abstractmethod
    def unwatch_strategy(self, target: WatchStrategy) -> bool:
        pass


class StockApi(abc.ABC):
    @abc.abstractmethod
    def get_stocks_in_market(self, market: Literal["kosqi", "kosdaq", "KOSPI", "KOSDAQ"]) -> List[Stock]:
        pass

    @abc.abstractmethod
    def watch_stock_price(self, target: WatchStock) -> bool:
        pass

    @abc.abstractmethod
    def unwatch_stock(self, target: WatchStock) -> bool:
        pass

    @abc.abstractmethod
    def sell(self, target: SellStock) -> bool:
        pass

    @abc.abstractmethod
    def buy(self, target: BuyStock) -> bool:
        pass


class MarketApi(StockApi, StrategyApi, ABC):

    @abc.abstractmethod
    def my_account(self):
        pass
