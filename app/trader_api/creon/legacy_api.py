from typing import List, Dict
import win32com.client
import win32com
import ctypes
from enum import Enum

"""
Stock Trader
1. Creon API 초기화
2. 거래 대상 종목 선정
3. LGrmfoa!5959@ 
"""
g_objCodeMgr = win32com.client.Dispatch('CpUtil.CpCodeMgr')
g_objCpCode = win32com.client.Dispatch('CpUtil.CpStockCode')
g_objCpStatus = win32com.client.Dispatch('CpUtil.CpCybos')
g_objCpTrade = win32com.client.Dispatch('CpTrade.CpTdUtil')


def InitPlusCheck():
    # 프로세스가 관리자 권한으로 실행 여부
    if ctypes.windll.shell32.IsUserAnAdmin():
        print('정상: 관리자권한으로 실행된 프로세스입니다.')
    else:
        print('오류: 일반권한으로 실행됨. 관리자 권한으로 실행해 주세요')
        return False

    # 연결 여부 체크
    if (g_objCpStatus.IsConnect == 0):
        print("PLUS가 정상적으로 연결되지 않음. ")
        return False

    # 주문 관련 초기화
    if (g_objCpTrade.TradeInit(0) != 0):
        print("주문 초기화 실패")
        return False

    return True


class StockStatus(Enum):
    IN = 0
    OUT = 1


class CpEvent:
    def set_params(self, client, name, caller):
        self.client = client  # CP 실시간 통신 object
        self.name = name  # 서비스가 다른 이벤트를 구분하기 위한 이름
        self.caller = caller  # callback 을 위해 보관

    def OnReceived(self):
        pbData = {}
        # 실시간 종목검색 감시 처리
        if self.name == 'cssalert':
            pbData['strategy_id'] = self.client.GetHeaderValue(0)
            pbData['monitor_id'] = self.client.GetHeaderValue(1)
            pbData['code'] = self.client.GetHeaderValue(2)
            pbData['name'] = g_objCpCode.CodeToName(pbData['code'])

            inoutflag = self.client.GetHeaderValue(3)
            if (ord('1') == inoutflag):
                pbData['status'] = StockStatus.IN.value
            elif (ord('2') == inoutflag):
                pbData['status'] = StockStatus.OUT.value
            pbData['catched_at'] = self.client.GetHeaderValue(4)
            pbData['current_value'] = self.client.GetHeaderValue(5)
            self.caller(pbData)

        elif self.name == 'stockcur':
            # 실시간 현재가.
            stock = {}
            stock['code'] = self.client.GetHeaderValue(0)  # 초
            stock['name'] = self.client.GetHeaderValue(1)  # 초
            stock['timess'] = self.client.GetHeaderValue(18)  # 초
            stock['exFlag'] = self.client.GetHeaderValue(19)  # 예상체결 플래그
            stock['cprice'] = self.client.GetHeaderValue(13)  # 현재가
            stock['diff'] = self.client.GetHeaderValue(2)  # 대비
            stock['cVol'] = self.client.GetHeaderValue(17)  # 순간체결수량
            stock['vol'] = self.client.GetHeaderValue(9)  # 거래량

            self.caller(stock)


class CpPublish:
    def __init__(self, name, serviceID):
        self.name = name
        self.obj = win32com.client.Dispatch(serviceID)
        self.bIsSB = False

    def Subscribe(self, var, caller):
        if self.bIsSB:
            self.Unsubscribe()

        if (len(var) > 0):
            self.obj.SetInputValue(0, var)

        handler = win32com.client.WithEvents(self.obj, CpEvent)
        handler.set_params(self.obj, self.name, caller)
        self.obj.Subscribe()
        self.bIsSB = True

    def Unsubscribe(self):
        if self.bIsSB:
            self.obj.Unsubscribe()
        self.bIsSB = False


class CpPBCssAlert(CpPublish):
    def __init__(self):
        super().__init__('cssalert', 'CpSysDib.CssAlert')


class CreonAPI:
    def __init__(self, on_updated_strategy_stock, on_changed_stock_price_fn):
        self.cp_alert = None
        self.objStockCur = None
        self._monitoring_s_id = None
        self._market_eye = win32com.client.Dispatch("CpSysDib.MarketEye")
        self._on_updated_strategy_stock = on_updated_strategy_stock
        self._on_changed_stock_price_fn = on_changed_stock_price_fn

        self.clear_monitor()

    def request_strategies(self, is_example: bool = True) -> List[Dict]:
        # TODO : 목룍을 예제에서 나의 것으로
        # CreonStrategyData(name=s['name'], s_id=s['id'], created_at=s['created_at'])
        retStgList = {}
        objRq = win32com.client.Dispatch("CpSysDib.CssStgList")

        # 예제 전략에서 전략 리스트를 가져옵니다.
        if is_example:
            objRq.SetInputValue(0, ord('0'))  # '0' : 예제전략, '1': 나의전략
        else:
            objRq.SetInputValue(0, ord('1'))  # '0' : 예제전략, '1': 나의전략
        objRq.BlockRequest()

        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            return []

        cnt = objRq.GetHeaderValue(0)  # 0 - (long) 전략 목록 수
        flag = objRq.GetHeaderValue(1)  # 1 - (char) 요청구분

        for i in range(cnt):
            item = {}
            item['name'] = objRq.GetDataValue(0, i)
            item['id'] = objRq.GetDataValue(1, i)
            item['created_at'] = objRq.GetDataValue(2, i)
            item['작성자필명'] = objRq.GetDataValue(3, i)
            item['평균종목수'] = objRq.GetDataValue(4, i)
            item['평균승률'] = objRq.GetDataValue(5, i)
            item['평균수익'] = objRq.GetDataValue(6, i)
            retStgList[item['name']] = item

        return retStgList

    def clear_monitor(self):
        delitem = []
        # for id, monId in self.monList.items():
        #     delitem.append((id, monId))
        #
        # for item in delitem:
        #     self.requestStgControl(item[0], item[1], False)

        if self.cp_alert is not None:
            self.cp_alert.Unsubscribe()
            self.cp_alert = None

    def clear_monitor_stock(self):
        if self.objStockCur is not None:
            self.objStockCur.Unsubscribe()

    def get_target_stocks(self, strategy_id):
        """
        TODO : 감시 중인 전략으로 찾아서 감시 중인 종목의 수가 200개를 넘어가면 안됨.
        따라서, 감시 전략의 종목을 누적 확인해서 200개가 넘지 않도록 해야함
        """
        retStgList = []
        objRq = win32com.client.Dispatch("CpSysDib.CssStgFind")
        objRq.SetInputValue(0, strategy_id)  # 전략 id 요청
        objRq.BlockRequest()
        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            # rqRet = objRq.GetDibMsg1()
            # return (False, retStgList)
            return []

        cnt = objRq.GetHeaderValue(0)  # 0 - (long) 검색된 결과 종목 수
        # totcnt = objRq.GetHeaderValue(1)  # 1 - (long) 총 검색 종목 수
        # stime = objRq.GetHeaderValue(2)  # 2 - (string) 검색시간
        # print('검색된 종목수:', cnt, '전체종목수:', totcnt, '검색시간:', stime)

        for i in range(cnt):
            item = {}
            item['code'] = objRq.GetDataValue(0, i)
            item['종목명'] = g_objCodeMgr.CodeToName(item['code'])
            retStgList.append(item)

        return retStgList

    def monitor(self, strategy_id, listener_fn):
        assert callable(listener_fn), "Listener must be Callable, but {}".format(type(listener_fn))

        if self._monitoring_s_id is not None:
            self.clear_monitor()
            self._monitoring_s_id = None

        objRq = win32com.client.Dispatch("CpSysDib.CssWatchStgSubscribe")
        objRq.SetInputValue(0, id)  # 전략 id 요청
        objRq.BlockRequest()

        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            # print("통신상태", rqStatus, rqRet)
            return False, "통신  상태 : {}, {}".format(rqStatus, rqRet)

        monitor_id = objRq.GetHeaderValue(0)
        if monitor_id == 0:
            return False, '감시 일련번호 구하기 실패'

        objRq = win32com.client.Dispatch("CpSysDib.CssWatchStgControl")
        objRq.SetInputValue(0, strategy_id)  # 전략 id 요청
        objRq.SetInputValue(1, monitor_id)  # 감시일련번호
        objRq.SetInputValue(2, ord('1'))  # 감시시작
        # objRq.SetInputValue(2, ord('3'))  # 감시취소
        objRq.BlockRequest()

        # 통신 및 통신 에러 처리
        rqStatus = objRq.GetDibStatus()
        if rqStatus != 0:
            rqRet = objRq.GetDibMsg1()
            return False, "통신  상태 : {}, {}".format(rqStatus, rqRet)

        if self.cp_alert is None:
            self.cp_alert = CpPBCssAlert()
            self.cp_alert.Subscribe('',  self.on_updated_monitor)

        status = objRq.GetHeaderValue(0)

        """ Status
        0 : 초기상태
        1 : 감시 중
        2 : 감시 중단
        3 : 등록 취소
        """
        if status == 0:
            msg = '전략감시상태: 초기상태'
        elif status == 1:
            msg = '전략감시상태: 감시중'
        elif status == 2:
            msg = '전략감시상태: 감시중단'
        elif status == 3:
            msg = '전략감시상태: 등록취소'

        self._monitoring_s_id = strategy_id

        return True, msg

    def on_updated_monitor(self, monitor, stock):
        print(monitor)
        print(stock)

    def on_catched_stock(self, stock):
        if callable(self._on_changed_stock_price_fn):
            self._on_changed_stock_price_fn(stock)

    def get_stocks_of_market(self, market_code: int = 1):
        """
        TODO
        :market_code: int (1: Kospi, 2: Kosdaq)
        :return:
        """
        stocks = []
        codes = g_objCodeMgr.GetStockListByMarket(market_code)
        total_length = len(codes)
        split_count = (total_length // 200) + (0 if total_length % 200 == 0 else 1)
        for s_i in range(split_count):
            begin_i = s_i * 200
            ret, _part_stocks = self.get_stocks_with_codes(codes[begin_i: begin_i + 200])
            stocks.extend(_part_stocks)

        return stocks

    def get_stocks_with_codes(self, stock_codes: List[int]) -> List:
        rq_fields = [0, 4, 17, 20]

        self._market_eye.SetInputValue(0, rq_fields)
        self._market_eye.SetInputValue(1, stock_codes)
        self._market_eye.BlockRequest()

        status = self._market_eye.GetDibStatus()
        if status != 0:
            return False, None

        cnt = self._market_eye.GetHeaderValue(2)
        stocks = []
        for i in range(cnt):
            code = self._market_eye.GetDataValue(0, i)
            last_price = self._market_eye.GetDataValue(1, i)
            name = self._market_eye.GetDataValue(2, i)
            total_stocks = self._market_eye.GetDataValue(3, i)

            data = {"code": code, "name": name, "last_price": last_price, "total_stocks": total_stocks}
            stocks.append(data)

        return True, stocks

    def monitor_stock(self, stock_code: str):
        if self.objStockCur is not None:
            self.objStockCur.Unsubscribe()
        else:
            self.objStockCur = win32com.client.Dispatch("DsCbo1.StockCur")
            handler = win32com.client.WithEvents(self.objStockCur, CpEvent)

        self.objStockCur.SetInputValue(0, stock_code)
        handler.set_params(self.objStockCur, stock_code, self.on_catched_stock)
        self.objStockCur.Subscribe()

