import abc
from typing import List, Dict, Literal, Optional
import win32com.client
import win32com
import ctypes
from enum import Enum
from app.trader_api.trader_api import Strategy, Stock, BuyStock, SellStock, WatchStrategy, WatchStock, MarketApi
from app.trader_api.trader_api import create_transaction_from_dict, create_stock_from_dict, IMarketResponse


def STATUS_CODE_TO_MESSAGE(code):
    if code == 0 :
        return '전략감시상태: 초기상태'
    elif code == 1 :
        return '전략감시상태: 감시중'
    elif code == 2 :
        return '전략감시상태: 감시중단'
    elif code == 3 :
        return '전략감시상태: 등록취소'


class CpEvent(abc.ABC):
    def set_params(self, client, name, caller):
        self._client = client
        self._name = name
        self._caller = caller

    @abc.abstractmethod
    def OnReceived(self):
        pass


class WatchStrategyEvent(CpEvent):
    def OnReceived(self):
        data = {'strategy_id': self._client.GetHeaderValue(0), 'monitor_id': self._client.GetHeaderValue(1),
                'code': self._client.GetHeaderValue(2), 'in': self.client.GetHeaderValue(3) == ord("1")}

        if callable(self._caller):
            self._caller(data)


class WatchStockEvent(CpEvent):
    def OnReceived(self):
        code = self._client.GetHeaderValue(0)  # 초
        name = self._client.GetHeaderValue(1)  # 초
        timess = self._client.GetHeaderValue(18)  # 초
        exFlag = self._client.GetHeaderValue(19)  # 예상체결 플래그 (1: 장전 동시호가(예상 체결), 2: 장중 체결)
        cprice = self._client.GetHeaderValue(13)  # 현재가
        diff = self._client.GetHeaderValue(2)  # 대비
        cVol = self._client.GetHeaderValue(17)  # 순간체결수량
        vol = self._client.GetHeaderValue(9)  # 거래량

        print("Watch Stock Event : {} / {}".format(name, code))
        if callable(self._caller):
            self._caller({"code": code, "name": name, "flag": exFlag == 2,
                          "at_time": timess, "current_price": cprice,
                          "diff": diff, "volume": cVol, "total_volume": vol})


class CpEventSubscriber:
    def __init__(self, name, service_id):
        self.name = name
        self.obj = win32com.client.Dispatch(service_id)
        self.is_subscribe = False

    def subscribe(self, var, caller, event: CpEvent.__class__, auto_unsubscribe=False) -> bool:
        if self.is_subscribe:
            if auto_unsubscribe:
                self.unsubscribe()
            else:
                return False

        if len(var) > 0:
            self.obj.SetInputValue(0, var)
        handler = win32com.client.WithEvents(self.obj, event)
        handler.set_params(self.obj, self.name, caller)
        self.obj.Subscribe()
        self.is_subscribe = True

        return True

    def unsubscribe(self):
        if self.is_subscribe:
            self.obj.Unsubscribe()
        self.is_subscribe = False


class CpStockPriceSubscriber:
    def __init__(self, target: WatchStock):
        self._stock_cur = win32com.client.Dispatch("DsCbo1.StockCur")
        self._target = target

    def has_target(self, target):
        return self._target == target

    def subscribe(self, caller):
        handler = win32com.client.WithEvents(self._stock_cur, WatchStockEvent)
        self._stock_cur.SetInputValue(0, self._target.code)
        handler.set_params(self._stock_cur, self._target.name, caller)
        self._stock_cur.Subscribe()

    def unsubscribe(self):
        self._stock_cur.Unsubscribe()


class _AccountAPI:
    def __init__(self, cp_trade_obj, account_id: int = 0):
        self._cp_trade_obj = cp_trade_obj
        self._account_id = account_id
        try:
            self._account = self._cp_trade_obj.AccountNumber[self._account_id]
        except Exception as e:
            print(e)
            print("Failed to init account information : id {}".format(self._account_id))

        # 주식상품 구분 (??)
        self._good_types = self._cp_trade_obj.GoodsList(self._account, 1)
        self._source_fund_dict = {ord(' '): '현금',
                                  ord('Y'): '융자',
                                  ord('D'): '대주',
                                  ord('B'): '담보',
                                  ord('M'): '매입담보',
                                  ord('P'): '플러스론',
                                  ord('I'): '자기융자'}

    def _request_account_info(self, trade_obj, to_count: int = 50):
        trade_obj.BlockRequest()
        request_status = trade_obj.GetDibStatus()
        if request_status != 0:
            print("failed to request Account Info")
            return None

        stock_counts = trade_obj.GetHeaderValue(7)
        print("get account stocks : {} / to counts {}".format(stock_counts, to_count))
        stocks = []
        for i in range(min(to_count, stock_counts)):
            code = trade_obj.GetDataValue(12, i)
            name = trade_obj.GetDataValue(0, i)
            source_fund = self._source_fund_dict[trade_obj.GetDataValue(1, i)]
            remain_count = trade_obj.GetDataValue(7, i)
            available_sell = trade_obj.GetDataValue(15, i)
            buy_unit_price = trade_obj.GetDataValue(17, i)
            buy_total_price = buy_unit_price * remain_count

            stocks.append({"code": code, "name": name, "source_fund": source_fund,
                           "remain_count": remain_count, "available_sell": available_sell,
                           "buy_unit_price": buy_unit_price, "buy_total_price": buy_total_price})

        return stocks

    def get_account_info(self, good_type_id: int = 0, num_stocks: int = 50):
        good_type = self._good_types[good_type_id]
        trade_obj = win32com.client.Dispatch("CpTrade.CpTd6033")
        trade_obj.SetInputValue(0, self._account)
        trade_obj.SetInputValue(1, good_type)
        trade_obj.SetInputValue(2, num_stocks)

        my_all_stocks = []
        while True:
            to_count = min(num_stocks, 200 - len(my_all_stocks))
            stocks = self._request_account_info(trade_obj, to_count=to_count)
            if stocks is None or len(stocks):
                break

            my_all_stocks.extend(stocks)
            if len(my_all_stocks) < 200:
                continue

            if not trade_obj.Continue:
                break


class CreonAPI(MarketApi):
    cp_stock_code = win32com.client.Dispatch('CpUtil.CpStockCode')
    # cp_market_eye = win32com.client.Dispatch("CpSysDib.MarketEye")
    cp_code_mgr = win32com.client.Dispatch('CpUtil.CpCodeMgr')
    cp_status = win32com.client.Dispatch('CpUtil.CpCybos')
    cp_trade = win32com.client.Dispatch('CpTrade.CpTdUtil')
    cp_stg_list = win32com.client.Dispatch("CpSysDib.CssStgList")
    cp_stg_find = win32com.client.Dispatch("CpSysDib.CssStgFind")

    # TODO :
    cp_watch_stg_subscribe = win32com.client.Dispatch("CpSysDib.CssWatchStgSubscribe")
    cp_watch_stg_control = win32com.client.Dispatch("CpSysDib.CssWatchStgControl")

    # INFO : Is this constant ?
    LIMIT_WATCH_STOCKS = 5

    def __init__(self, market_response, name="Creon", limit_watch_stock=200):
        self._name = name
        # self._on_updated_strategy_stock = on_updated_strategy_stock
        # self._on_updated_stock_price = on_updated_stock_price
        self._market_response = market_response
        self._limit_watch_stock = limit_watch_stock

        self._strategy_subscriber = CpEventSubscriber('cssalert', 'CpSysDib.CssAlert')
        self._watching_strategies = []

        self._stock_price_subscribers = []

    @staticmethod
    def is_valid_service() -> [bool, str]:
        if not ctypes.windll.shell32.IsUserAnAdmin():
            return False, '오류: 일반권한으로 실행됨. 관리자 권한으로 실행해 주세요'

        if CreonAPI.cp_status.IsConnect == 0:
            return False, "PLUS가 정상적으로 연결되지 않음."

        if CreonAPI.cp_trade.TradeInit(0) != 0:
            return False, "주문 초기화 실패"

        return True, "CREON PLUS가 정상 연결"

    def get_stocks_in_market(self, market: Literal["kosqi", "kosdaq", "KOSPI", "KOSDAQ"]) -> List[Stock]:
        pass

    def on_updated_stock_price(self, transaction: Dict):
        if isinstance(self._market_response, IMarketResponse):
            self._market_response.on_watched_stock_price(create_transaction_from_dict(transaction))

    def watch_stock_price(self, target: WatchStock) -> bool:
        if self.LIMIT_WATCH_STOCKS == len(self._stock_price_subscribers):
            return False

        subscriber = CpStockPriceSubscriber(target)
        subscriber.subscribe(self.on_updated_stock_price)

        self._stock_price_subscribers.append(subscriber)

        return True

    def get_strategies(self, is_test=True) -> List[Strategy]:
        api = CreonAPI.cp_stg_list
        api.SetInputValue(0, ord('0' if is_test else '1'))
        api.BlockRequest()

        result = api.GetDibStatus()
        if result != 0:
            return []

        total_count = api.GetHeaderValue(0)
        strategies = []
        for i in range(total_count):
            s = Strategy(
                name=api.GetDataValue(0, i),
                strategy_id=api.GetDataValue(1, i),
                created_at=api.GetDataValue(2, i),
                owner=api.GetDataValue(3, i),
                avg_stocks=api.GetDataValue(4, i),
                avg_win_rate=api.GetDataValue(5, i),
                avg_income=api.GetDataValue(6, i)
            )
            strategies.append(s)

        return strategies

    def _find_strategy_stocks(self, target: WatchStrategy) -> List[Stock]:
        api = CreonAPI.cp_stg_find
        api.SetInputValue(0, target.strategy_id)
        api.BlockRequest()

        if api.GetDibStatus() != 0:
            raise ValueError("통신 상태 안좋음")

        cnt = api.GetHeaderValue(0)  # 0 - (long) 검색된 결과 종목 수
        # totcnt = api.GetHeaderValue(1)  # 1 - (long) 총 검색 종목 수
        # stime = api.GetHeaderValue(2)  # 2 - (string) 검색시간

        stocks = []
        for i in range(cnt):
            code = api.GetDataValue(0, i)
            s = Stock(name=CreonAPI.cp_code_mgr.CodeToName(code), code=code, current_value=0)
            stocks.append(s)

        return stocks

    def find_strategy_monitor_id(self, target: WatchStrategy) -> str:
        subscribe_api = self.cp_watch_stg_subscribe
        subscribe_api.SetInputValue(0, target.strategy_id)
        subscribe_api.BlockRequest()

        if subscribe_api.GetDibStatus() != 0:
            raise ValueError("Failed to request Subscribe")

        monitor_id = subscribe_api.GetHeaderValue(0)
        if monitor_id == 0:
            raise ValueError("Failed to create 감시 일련 번호(Monitor ID)")

        return monitor_id

    def _request_watch_strategy_api(self, strategy_id, monitor_id, is_start=True):
        control_api = self.cp_watch_stg_control

        control_api.SetInputValue(0, strategy_id)  # 전략 id 요청
        control_api.SetInputValue(1, monitor_id)  # 감시일련번호
        control_api.SetInputValue(2, ord('1' if is_start else '3'))
        control_api.BlockRequest()
        if control_api.GetDibStatus() != 0:
            raise ValueError("Failed to request Monitor Strategy")
        status = control_api.GetHeaderValue(0)
        print("감시 상태 : {}".format(STATUS_CODE_TO_MESSAGE(status)))

        return True

    def stop_watch_strategy(self, monitor_id: str, strategy_id: str) -> bool:
        print(">>> stop watch : {}, {}".format(monitor_id, strategy_id))
        self._request_watch_strategy_api(monitor_id=monitor_id, strategy_id=strategy_id, is_start=False)
        for strategy in self._watching_strategies:
            if strategy.monitor_id == monitor_id and strategy.strategy_id == strategy_id:
                self._watching_strategies.remove(strategy)
                break

        return True

    def on_updated_strategy_stock(self, watched_stock: Dict):
        if isinstance(self._market_response, IMarketResponse):
            name = self.cp_stock_code.CodeToName(watched_stock['code'])
            stock = Stock(name, watched_stock['code'])
            strategy = Strategy(strategy_id=watched_stock['strategy_id'],
                                monitor_id=watched_stock['monitor_id'])

            self._market_response.on_watched_strategy_stock(stock, strategy)

    def start_watch_strategy(self, monitor_id: str, strategy_id: str) -> bool:
        self._request_watch_strategy_api(strategy_id, monitor_id, is_start=True)
        self._strategy_subscriber.subscribe('',
                                            self.on_updated_strategy_stock,
                                            WatchStrategyEvent,
                                            auto_unsubscribe=False)

        return True

    def watch_strategy(self, target: WatchStrategy) -> Optional[List[Stock]]:
        if target in self._watching_strategies:
            return None

        _stocks = self._find_strategy_stocks(target)
        if len(_stocks) > self._limit_watch_stock:
            raise OverflowError("Watch limit count overflow : {} > max {}".format(len(_stocks), self._limit_watch_stock))

        result = self.start_watch_strategy(target.monitor_id, target.strategy_id)
        self._watching_strategies.append(target)

        return _stocks


    """
    TODO : Sell, Buy
    """

    def my_account(self):
        pass

    def unwatch_stock(self, target: WatchStock) -> bool:
        target_subscriber = None
        for subscriber in self._stock_price_subscribers:
            if subscriber.has_target(target):
                subscriber.unsubscribe()
                target_subscriber = subscriber

        if target_subscriber is None:
            return False

        self._stock_price_subscribers.remove(target_subscriber)
        return True

    def unwatch_strategy(self, target: WatchStrategy) -> bool:
        if target not in self._watching_strategies:
            return False

        print(">>> unwatch strategy : {}".format(target))
        self.stop_watch_strategy(target.monitor_id, target.strategy_id)

    def sell(self, target: SellStock) -> bool:
        pass

    def buy(self, target: BuyStock) -> bool:
        pass

"""
        # 관심종목 객체 구하기
        objRq = win32com.client.Dispatch("CpSysDib.MarketEye")
        # 요청 필드 세팅 - 종목코드, 종목명, 시간, 대비부호, 대비, 현재가, 거래량

        # INFO : Code 유효성 검사 측면으로 보임.
        # if request_fields is None:
        #     request_fields = [0, 17, 1, 2, 3, 4, 10]
        # objRq.SetInputValue(0, request_fields) # 요청 필드
        # objRq.SetInputValue(1, codes)  # 종목코드 or 종목코드 리스트
        # objRq.BlockRequest()
        #
        #
        # # 현재가 통신 및 통신 에러 처리
        # rqStatus = objRq.GetDibStatus()
        # rqRet = objRq.GetDibMsg1()
        # print("통신상태", rqStatus, rqRet)
        # if rqStatus != 0:
        #     return False

        # cnt = objRq.GetHeaderValue(2)
        # for i in range(cnt):
        #     rpCode = objRq.GetDataValue(0, i)  # 드
        #     rpName = objRq.GetDataValue(1, i)  # 종목명
        #     rpTime= objRq.GetDataValue(2, i)  # 시간
        #     rpDiffFlag = objRq.GetDataValue(3, i)  # 대비부호
        #     rpDiff = objRq.GetDataValue(4, i)  # 대비
        #     rpCur = objRq.GetDataValue(5, i)  # 현재가
        #     rpVol = objRq.GetDataValue(6, i)  # 거래량

"""