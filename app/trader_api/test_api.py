import time
from typing import Dict, List, Literal, Optional
from app.trader_api.trader_api import Strategy, Stock, MarketApi
from app.trader_api.trader_api import WatchStock, BuyStock, SellStock, WatchStrategy, IMarketResponse


class TesterApi(MarketApi):
    def find_strategy_monitor_id(self, target: WatchStrategy) -> Optional[str]:
        return "1004"

    def __init__(self, market_response: IMarketResponse):
        self._market_response = market_response

        self._strategies = [Strategy(strategy_id="1004", name="나의 전략 #1", created_at="20150102"),
                            Strategy(strategy_id="1005", name="나의 전략 #2", created_at="20150102")]
        self._target_stocks = {"1004": [], "1005": []}
        self._stocks = [{"name": "삼성전자", "code": "A00001", "last_price": 12321}, {"name": "삼진엘앤디", "code": "B00002", "last_price": 12321}, {"name": "현대중공업", 'code': "C0002", "last_price": 12321}]

    def get_strategies(self, is_test: bool) -> List[Strategy]:
        return self._strategies

    def watch_strategy(self, target: WatchStrategy) -> Optional[List[Stock]]:
        ss = []
        for s in self._stocks:
            stock = Stock(name=s['name'], code=s['code'], current_value=s['last_price'])
            self._market_response.on_watched_strategy_stock(Stock(name=s['name'],
                                                                  code=s['code'],
                                                                  current_value=s['last_price']),
                                                            Strategy(strategy_id=target.strategy_id,
                                                                     name=target.name,
                                                                     monitor_id=target.monitor_id,
                                                                     created_at=str(int(time.time()*1000))))
            ss.append(stock)

        return ss

    def unwatch_strategy(self, target: WatchStrategy) -> bool:
        pass

    def get_stocks_in_market(self, market: Literal["kosqi", "kosdaq", "KOSPI", "KOSDAQ"]) -> List[Stock]:
        pass

    def watch_stock_price(self, target: WatchStock) -> bool:
        return True

    def unwatch_stock(self, target: WatchStock) -> bool:
        return True

    def sell(self, target: SellStock) -> bool:
        pass

    def buy(self, target: BuyStock) -> bool:
        pass
