from typing import Dict, List, Literal, Optional
from enum import Enum
from app.trader_api.data.strategy import CreonStrategyData
from app.trader_api.data.stock import CreonStock
from app.trader_api.trader_api import Strategy, Stock, StrategyApi, StockApi, IMarketResponse
from app.trader_api.trader_api import WatchStock, BuyStock, SellStock, WatchStrategy
from app.trader_api.trader_api import Transaction
from app.trader_api.factory_api import create_trader_api


class Market(Enum):
    KOSPI = 1
    KOSDAQ = 2


class TRADERS(Enum):
    test = 0
    creon = 1
    hi = 2


class Client(IMarketResponse):

    def __init__(self, trader: str = TRADERS.creon.name):
        self._trader = trader
        self._trader_api = create_trader_api(trader, self)
        self._on_updated_strategy_stock_listener = None
        self._on_updated_stock_price_listener = None
        self._market_stocks = {}
        self._current_watching_strategy = None

    def set_on_updated_strategy_stock_listener(self, listener_fn):
        self._on_updated_strategy_stock_listener = listener_fn

    def set_on_updated_stock_price_listener(self, listener_fn):
        self._on_updated_stock_price_listener = listener_fn

    def on_watched_stock_price(self, transaction: Transaction):
        print(">>> on watched stock price")
        print(transaction)
        if callable(self._on_updated_stock_price_listener):
            ## TODO : Transaction -> View Data
            self._on_updated_stock_price_listener(transaction)

    def on_watched_strategy_stock(self, stock: Stock, strategy: Strategy):
        if callable(self._on_updated_strategy_stock_listener):
            self._on_updated_strategy_stock_listener(CreonStock(name=stock.name, code=stock.code,
                                                                current_value=stock.current_value,
                                                                strategy_id=strategy.strategy_id,
                                                                monitor_id=strategy.monitor_id))

    def get_strategies(self, caller):
        strategies = self._trader_api.get_strategies(is_test=False)

        def convert_to_creon(strategy: Strategy):
            return CreonStrategyData(name=strategy.name, s_id=strategy.strategy_id, created_at=strategy.created_at)

        if caller is not None:
            caller([convert_to_creon(s) for s in strategies])

    def convert_creon_stock(self, strategies: List[Stock]) -> List[CreonStock]:
        data = []
        for s in strategies:
            data.append(CreonStock(name=s.name, code=s.code, current_value=s.current_value))

        return data

    def start_monitoring_strategy(self, strategy: CreonStrategyData) -> Optional[List[CreonStock]]:
        if self._current_watching_strategy is not None:
            self.stop_all_monitoring_strategy()

        target = WatchStrategy(name=strategy.strategy_name, strategy_id=strategy.strategy_id)
        monitor_id = self._trader_api.find_strategy_monitor_id(target)
        print("{} monitor : {}".format(target, monitor_id))
        # TODO : Monitor id 반영
        watched_strategy = target.update_monitor_id(monitor_id)

        stocks = self._trader_api.watch_strategy(watched_strategy)
        if stocks is None:
            return None
        self._current_watching_strategy = watched_strategy

        return self.convert_creon_stock(stocks)

    def stop_all_monitoring_strategy(self):
        if self._current_watching_strategy is not None:
            self._trader_api.unwatch_strategy(self._current_watching_strategy)
            self._current_watching_strategy = None

    def get_stocks(self, market: Market = Market.KOSPI) -> List[Dict]:
        # if market.name not in self._market_stocks:
            # self._market_stocks[market.name] = self._trader_api.get_stocks_of_market(market.value)

        return self._market_stocks[market.name]

    def search_stocks_with_name(self, name: str, market: Market):
        all_market_stocks = self.get_stocks(market)
        page_count = 100
        total_page = len(self._market_stocks[market.name]) // page_count + (0 if len(self._market_stocks) % page_count == 0 else 1)
        filtered = []
        for t_i in range(total_page):
            begin = page_count * t_i
            parts = all_market_stocks[begin: begin + page_count]
            for stock in parts:
                if name in stock['name']:
                    data = Stock(stock['name'], stock['code'], stock['last_price'])
                    filtered.append(data)

        return filtered

    def start_watching_stock(self, stock: CreonStock) -> bool:
        return self._trader_api.watch_stock_price(WatchStock(name=stock.name,
                                                             code=stock.code))

    def stop_watching_stock_price(self, stock: CreonStock) -> bool:
        return self._trader_api.unwatch_stock(WatchStock(name=stock.name,
                                                         code=stock.code))

