from typing import Optional
from app.trader_api.trader_api import MarketApi, IMarketResponse


def create_trader_api(trader_name: str, market_response: IMarketResponse) -> MarketApi:
    if trader_name.lower() == "creon":
        from app.trader_api.creon.creon_api import CreonAPI
        return CreonAPI(market_response)
    else:
        from app.trader_api.test_api import TesterApi
        return TesterApi(market_response)

