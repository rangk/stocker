import sys
from PyQt5.QtWidgets import *
from PyQt5 import uic
from PyQt5.QtCore import QThread, QObject, pyqtSignal, pyqtSlot

from app.utils.resource_provider import ResourceProvider
from app.trader_api.client import Client, Market
from app.views.base_view import *
from app.views.monitor_status_button import MonitorStatusView
from app.views.strategy_list_view import StrategyListView, ListViewWrapper
from app.services.telegram import TelegramBot, OnTelegramCommandListener
from app.trader_api.data.strategy import StrategyData
from app.trader_api.data.stock import Stock, CreonStock

resource_provider = ResourceProvider()

main_ui_file = resource_provider.get_ui_path("main")
MainUi = uic.loadUiType(main_ui_file)[0]


class OnCommandListener(QObject):
    get_strategy_signal = pyqtSignal()
    watch_strategy_signal = pyqtSignal(int)
    watch_stock_price_signal = pyqtSignal(int)

    def on_get_strategies(self):
        self.get_strategy_signal.emit()

    def watch_strategy(self, value: int):
        self.watch_strategy_signal.emit(value)

    def watch_stock_price(self, stock_number: int):
        self.watch_stock_price_signal.emit(stock_number)


class MainWindow(QMainWindow, MainUi):
    def __init__(self):
        super().__init__()
        self.setupUi(self)

        self._client = Client(trader="creon")
        self._client.set_on_updated_strategy_stock_listener(self._on_updated_strategy_stock)
        self._client.set_on_updated_stock_price_listener(self._on_updated_stock_price)

        self._label_market_status = LabelWrapper(self.label_market_status)
        self.update_market_status()

        self._strategy_list_view = StrategyListView(self.list_strategy)
        self._strategy_list_view.set_on_item_selected_listener(self.on_selected_strategy)

        self._btn_monitor = ButtonWrapper(self.btn_monitor)
        self._btn_monitor.set_enable(False)
        self._btn_monitor.set_on_clicked_listener(self._on_clicked_start_watching_strategy)

        self._text_stock = TextEditWrapper(self.text_stocks)
        self._btn_search_stock = ButtonWrapper(self.btn_search_stock)
        self._btn_search_stock.set_on_clicked_listener(self.on_clicked_search_stock)

        self._list_stocks = ListViewWrapper(self.list_stocks)
        self._list_stocks.set_on_item_selected_listener(self._on_selected_stock)
        self._list_monitoring_stocks = ListViewWrapper(self.list_monitoring_stocks)
        self._list_monitoring_stocks.set_on_item_selected_listener(self._on_selected_monitoring_stock)

        self._btn_monitor_stock = ButtonWrapper(self.btn_watch_stock)
        self._btn_monitor_stock.set_on_clicked_listener(self.on_clicked_watch_stock_price)
        self._btn_stop_monitor_stock = ButtonWrapper(self.btn_unwatch_stock)
        self._btn_stop_monitor_stock.set_on_clicked_listener(self.on_clicked_unwatch_stock_price)

        self._command_listener = OnCommandListener(self)
        self._command_listener.get_strategy_signal.connect(self.send_strategies)
        self._command_listener.watch_strategy_signal.connect(self.watch_strategy_from_telegram)
        self._command_listener.watch_stock_price_signal.connect(self.watch_stock_price_from_telegram)
        self._telegram_bot = TelegramBot(self._command_listener)
        self._telegram_bot.start_polling()
        #
        self._label_status = MonitorStatusView(self.label_status)
        self._label_status.stop_monitoring()
        #
        # # INFO : Components to monitor stocks
        #
        # self._spin_limit_monitor_count = SpinBoxWrapper(self.spin_limit_monitor_count)

        self._strategies = []
        self._selected_strategy = None
        self._selected_stock = None
        self._selected_monitoring_stock = None

        self._client.get_strategies(self.on_updated_strategies)

    def on_updated_strategies(self, strategies):
        self._strategy_list_view.remove_all_items()
        self._strategies = strategies
        self._strategy_list_view.add_items(self._strategies)

    def update_market_status(self):
        is_open = True  #TODO check market
        self._label_market_status.set_text("주식 거래중" if is_open else "주식 장 마감")
        self._label_market_status.set_text_color("#0F0" if is_open else "#F00")

    """
    TODO : Stock Monitroing 과 Strategy Monitoring의 기능 분리 필요 
    """

    def monitor_stock_price(self, stock_number):
        """

        :param stock_number: 1 ~
        :return:
        """
        result = self._list_stocks.select_item_by_index(stock_number - 1)
        if result:
            self.on_start_monitor_stock()
        else:
            self._telegram_bot.send_message("감시 실패 : 종목 검색 결과 번호를 정확히 입력해주세요.")

    def on_clicked_watch_stock_price(self):
        print("selected stock : {}".format(self._selected_stock))
        if self._selected_stock is not None:
            target_stock = self._selected_stock
            # If monitoring_count > limit_count:
            self.unwatch_stock_price()
            is_result = self._client.start_watching_stock(target_stock)
            if is_result:
                self._list_monitoring_stocks.add_item(self._selected_stock)
                self._list_stocks.remove_item(self._selected_stock)
                self._selected_stock = self._list_stocks.get_selected_item()

                self._telegram_bot.send_message("종목 감시 시작 : {}".format(target_stock))

    def on_clicked_unwatch_stock_price(self):
        self.unwatch_stock_price()

    def unwatch_stock_price(self):
        if len(self._list_monitoring_stocks.get_items()) == 0:
            return True

        monitoring_items = self._list_monitoring_stocks.get_items()
        self._selected_monitoring_stock = monitoring_items[-1]
        is_result = self._client.stop_watching_stock_price(self._selected_monitoring_stock)
        if is_result:
            self._list_stocks.add_item(self._selected_monitoring_stock)
            self._list_monitoring_stocks.remove_item(self._selected_monitoring_stock)
        else:
            # TODO : Alert
            pass

        # self._selected_monitoring_stock = self._list_monitoring_stocks.get_selected_item()
        # self._telegram_bot.send_message("감시 정지 : {}".format(target_stock))

        return True

    def clear_duplicated(self, stocks):
        monitoring_stocks = self._list_monitoring_stocks.get_items()
        if len(monitoring_stocks) == 0:
            return stocks

        _filtered = []
        for s in stocks:
            is_duplicated = False
            for m_s in monitoring_stocks:
                if s == m_s:
                    monitoring_stocks.remove(m_s)
                    is_duplicated = True
                    break
            if not is_duplicated:
                _filtered.append(s)

        return _filtered

    def search_stocks(self, search_value):
        self._text_stock.set_text(search_value)
        self.on_clicked_search_stock()

    def on_clicked_search_stock(self):
        value = self._text_stock.get_text()
        self._list_stocks.filter_like(value, lambda_fn=lambda x: x.name)

        # self._list_stocks.remove_all_items()
        #
        # name = self._text_stock.get_text()
        # stocks = self._client.search_stocks_with_name(name, Market.KOSPI)
        # clean_stocks = self.clear_duplicated(stocks)
        #
        # if len(clean_stocks) == 0:
        #     print("not found stocks !!")
        #
        #     # Refacotring
        #     self._telegram_bot.send_message("해당 조검({})의 검색 결과가 없습니다.".format(name))
        # else:
        #     self._list_stocks.add_items(clean_stocks)
        #
        #     # Refacotring
        #     message = "[종목 검색 결과 : {}]".format(len(clean_stocks))
        #     message = "{}\n이미 감시 중인 종목을 제외하고 보여집니다.\n번호 | 코드 | 종목명 | 현재가격".format(message)
        #     for s_i, s in enumerate(clean_stocks):
        #         message = "{}\n{}".format(message, "번호 {} : {}".format(s_i+1, s))
        #     self._telegram_bot.send_message(message)

    def _on_selected_monitoring_stock(self, stock):
        self._selected_monitoring_stock = stock

    def _on_selected_stock(self, stock):
        self._selected_stock = stock

    def watch_strategy_from_telegram(self, strategy_id):
        self._on_clicked_stop_watching_strategy()
        if not self._strategy_list_view.select_item_by_index(strategy_id):
            return

        self._on_clicked_start_watching_strategy()

    def watch_stock_price_from_telegram(self, stock_id):
        if not self._list_stocks.select_item_by_index(stock_id):
            return

        self.on_clicked_watch_stock_price()

    def send_strategies(self):
        strategies = self._strategy_list_view.get_items()
        if len(strategies) == 0:
            self._telegram_bot.send_message("감시 가능한 전략이 없습니다.")
            return

        message = "[ 나의 감시 전략 ] : {}개\n".format(len(strategies))
        self._telegram_bot.send_message(message)
        message = ""
        for s_i, s in enumerate(strategies):
            message = message + "감시 전략 {} : {}\n".format(s_i, s.strategy_name)
            if s_i > 0 and s_i % 10 == 0:
                self._telegram_bot.send_message(message)
                message = ""
        if len(message) > 0:
            self._telegram_bot.send_message(message)

    def _on_updated_stock_price(self, stock: Stock):
        self._telegram_bot.send_message("종목 가격 감지\n{} 종목 : 감지 가격 {}".format(stock.name, stock.code))

    def _on_clicked_all_stop(self):
        self._client.stop_all_monitoring_strategy()
        self._strategy_list_view.clear_selected()
        self._list_monitoring.remove_all_items()
        self._list_catched_stocks.remove_all_items()

        self._btn_monitor.set_enable(False)
        self._btn_all_stop.set_enable(False)

        self._telegram_bot.send_message("모든 전략 감시를 종료했습니다.")

    def on_selected_strategy(self, selected_strategy: StrategyData):
        assert isinstance(selected_strategy, StrategyData), \
            "ValueError : {} is not CreonSt rategyData".format(type(selected_strategy))
        self._btn_monitor.set_enable(True)
        self._selected_strategy = selected_strategy

    def _update_watch_strategy_status(self, is_watching):
        if is_watching:
            self._label_status.start_monitoring()
            self._strategy_list_view.set_enable(False)
            self._btn_monitor.set_text("감시 중지")
            self._btn_monitor.set_on_clicked_listener(self._on_clicked_stop_watching_strategy)
        else:
            self._label_status.stop_monitoring()
            self._strategy_list_view.set_enable(True)
            self._btn_monitor.set_text("감시 시작")
            self._btn_monitor.set_on_clicked_listener(self._on_clicked_start_watching_strategy)

    def _on_clicked_stop_watching_strategy(self):
        self._client.stop_all_monitoring_strategy()
        self._update_watch_strategy_status(False)

        self._list_stocks.remove_all_items()
        self._telegram_bot.send_message("전략 감시가 중단 되었습니다.")

    def _on_clicked_start_watching_strategy(self):
        stocks = None
        try:
            stocks = self._client.start_monitoring_strategy(self._selected_strategy)
            self._update_watch_strategy_status(stocks is not None)
        except Exception as e:
            print(e)

        if stocks is not None:
            self._telegram_bot.send_message("{} 전략의 감시를 시작합니다.".format(self._selected_strategy))
        else:
            self._telegram_bot.send_message("{} 전략 감시를 시작하지 못 했습니다. 오류가 발생 했습니다.".format(self._selected_strategy))

        self._list_stocks.add_items(stocks)
        message = "[ 감지 종목 ] 총 {}개".format(len(stocks))
        self._telegram_bot.send_message(message)
        message = ""
        for s_i, s in enumerate(stocks):
            message = message + "{}번 : {} | {}\n".format(s_i, s.code, s.name)
            if s_i > 0 and s_i % 10 == 0:
                self._telegram_bot.send_message(message)
                message = ""
        if len(message) > 0:
            self._telegram_bot.send_message(message)

    def _on_updated_strategy_stock(self, stock: CreonStock):
        self._list_stocks.add_item(stock)
        # self._telegram_bot.send_message(
        #     "전략 감지 : 전략 {} / {} (감지 시간 : {})".format(stock.strategy_id, stock.name, stock.catch_at))


app = QApplication(sys.argv)

window = MainWindow()
window.show()
app.exec_()
