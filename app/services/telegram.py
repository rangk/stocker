import telebot
import threading
import abc

"""
Command Example
{
   "content_type":"text",
   "id":4896,
   "message_id":4896,
   "from_user":{
      "id":523158531,
      "is_bot":false,
      "first_name":"JungGyu",
      "username":"None",
      "last_name":"Kim",
      "language_code":"ko",
      "can_join_groups":"None",
      "can_read_all_group_messages":"None",
      "supports_inline_queries":"None"
   },
   "date":1612082740,
   "chat":{
      "id":523158531,
      "type":"private",
      "title":"None",
      "username":"None",
      "first_name":"JungGyu",
      "last_name":"Kim",
      "photo":"None",
      "bio":"None",
      "description":"None",
      "invite_link":"None",
      "pinned_message":"None",
      "permissions":"None",
      "slow_mode_delay":"None",
      "sticker_set_name":"None",
      "can_set_sticker_set":"None",
      "linked_chat_id":"None",
      "location":"None"
   },
   "forward_from":"None",
   "forward_from_chat":"None",
   "forward_from_message_id":"None",
   "forward_signature":"None",
   "forward_sender_name":"None",
   "forward_date":"None",
   "reply_to_message":"None",
   "edit_date":"None",
   "media_group_id":"None",
   "author_signature":"None",
   "text":"/get",
   "entities":[
      <telebot.types.MessageEntity object at 0x1168f3850>
   ],
   "caption_entities":"None",
   "audio":"None",
   "document":"None",
   "photo":"None",
   "sticker":"None",
   "video":"None",
   "video_note":"None",
   "voice":"None",
   "caption":"None",
   "contact":"None",
   "location":"None",
   "venue":"None",
   "animation":"None",
   "dice":"None",
   "new_chat_member":"None",
   "new_chat_members":"None",
   "left_chat_member":"None",
   "new_chat_title":"None",
   "new_chat_photo":"None",
   "delete_chat_photo":"None",
   "group_chat_created":"None",
   "supergroup_chat_created":"None",
   "channel_chat_created":"None",
   "migrate_to_chat_id":"None",
   "migrate_from_chat_id":"None",
   "pinned_message":"None",
   "invoice":"None",
   "successful_payment":"None",
   "connected_website":"None",
   "reply_markup":"None",
   "json":{
      "message_id":4896,
      "from":{
         "id":523158531,
         "is_bot":false,
         "first_name":"JungGyu",
         "last_name":"Kim",
         "language_code":"ko"
      },
      "chat":{
         "id":523158531,
         "first_name":"JungGyu",
         "last_name":"Kim",
         "type":"private"
      },
      "date":1612082740,
      "text":"/get",
      "entities":[
         {
            "offset":0,
            "length":4,
            "type":"bot_command"
         }
      ]
   }
}
"""


class OnTelegramCommandListener(abc.ABC):
    @abc.abstractmethod
    def on_get(self, message):
        pass

    @abc.abstractmethod
    def on_start(self, message):
        pass

    @abc.abstractmethod
    def on_stop(self, message):
        pass

    @abc.abstractmethod
    def on_sell(self, message):
        pass

    @abc.abstractmethod
    def on_search(self, value):
        pass


class TelegramBot:
    """
    전체 사용 방법은 참조 :
      * Telegram 홈페이지 : https://core.telegram.org/bots
      * Github : https://github.com/eternnoir/pyTelegramBotAPI

    TELEGRAM_BOT_TOKEN : Telegram 홈페이지에서 생성한 CHAT BOT API Token
    CHAT_ID : CHAT BOT 이 생성한 Chat 방의 ID
    """
    TELEGRAM_BOT_TOKEN = "711891903:AAE8yXihYax9uem4cooJEH6Z7p6oqyhrZM8"
    CHAT_ID = "523158531"

    _chat_bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN)

    def __init__(self, on_command_listener):
        super(TelegramBot, self).__init__()
        setattr(TelegramBot._chat_bot, 'on_command_listener', on_command_listener)

    @staticmethod
    @_chat_bot.message_handler(commands=['get'])
    def request_get_strategies(message):
        print("GET Strategies")
        TelegramBot._chat_bot.on_command_listener.on_get_strategies()

    @staticmethod
    @_chat_bot.message_handler(commands=['wstg'])
    def request_watch_strategy(message):
        stg_id = int(message.text.split(" ")[1])
        TelegramBot._chat_bot.on_command_listener.watch_strategy(stg_id)

    @staticmethod
    @_chat_bot.message_handler(commands=['wst'])
    def request_watch_stock_price(message):
        stock_id = int(message.text.split(" ")[1])
        TelegramBot._chat_bot.on_command_listener.watch_stock_price(stock_id)

    # @staticmethod
    # @_chat_bot.message_handler(commands=['stop'])
    # def request_get_strategies(message):
    #     print("Stop")
    #
    # @staticmethod
    # @_chat_bot.message_handler(commands=['sell'])
    # def request_get_strategies(message):
    #     print("sell")
    #
    # @staticmethod
    # @_chat_bot.message_handler(commands=['moni_stock'])
    # def request_moni_stock(message):
    #     s_number = message.text.split(" ")[1]
    #     TelegramBot._chat_bot.on_command_listener.on_monitor_stock_price(s_number)
    #
    # @staticmethod
    # @_chat_bot.message_handler(commands=['search'])
    # def request_get_strategies(message):
    #     value = message.text.split(" ")[1]
    #     TelegramBot._chat_bot.on_command_listener.on_search(value)

    @classmethod
    def send_message(cls, message):
        try:
            cls._chat_bot.send_message(cls.CHAT_ID, message)

        except ConnectionError as ce:
            cls._error_log.write('Telegram Failed to send message : {}'.format(ce))
        except Exception as e:
            cls._error_log.write('Telegram Failed to send message : {}'.format(e))

    # TODO : image path 가 아닌 이미지 자체를 보낼 수 있도록 작업해야함.
    @classmethod
    def send_image(cls, image_path):
        try:
            cls._chat_bot.send_photo(cls.CHAT_ID, open(image_path, 'rb'))

        except ConnectionError as ce:
            cls._error_log.write('Telegram Failed to send image : {}'.format(ce))
        except Exception as e:
            cls._error_log.write('Telegram Failed to send image : {}'.format(e))

    @staticmethod
    def polling():
        print("start polling!!!")
        try:
            TelegramBot._chat_bot.polling()
        except:
            pass
        # except ConnectionError as ce:
        #     TelegramCommunicator._error_log.error('ConnectionError: {}'.format(ce))
        # except Exception as e:
        #     TelegramCommunicator._error_log.error('ConnectionError: {}'.format(e))

    @classmethod
    def start_polling(cls):
        thread = threading.Thread(target=TelegramBot.polling, args={}, kwargs={}, daemon=True)
        thread.start()

