from typing import List
from app.views.base_view import ListViewWrapper
from app.trader_api.data.strategy import StrategyData


class StrategyListView(ListViewWrapper):
    def add_item(self, item: StrategyData):
        super(StrategyListView, self).add_item(item)

    def add_items(self, items: List[StrategyData]):
        for item in items:
            self.add_item(item)
