from typing import List, Callable, Union
from PyQt5.QtWidgets import *
import copy


class LabelWrapper:
    def __init__(self, label_view: QLabel):
        self._label_view = label_view

    def set_bold(self, is_bold):
        self._label_view.font().setBold(is_bold)

    def set_text(self, text: str):
        self._label_view.setText(text)

    def set_text_color(self, color: str):
        self._label_view.setStyleSheet("color:{}".format(color))


class ListViewWrapper:
    def __init__(self, qt_list_view: QListView):
        self._view = qt_list_view
        self._items = []
        self._listener_fn = None
        self._selected_row = None
        self._view.currentItemChanged.connect(self._on_selected_item)

    def _on_selected_item(self):
        self._selected_row = self._view.currentRow()
        if self._listener_fn is not None:
            self._listener_fn(self._items[self._selected_row] if self._selected_row >= 0 else None)

    def add_item(self, text):
        self._view.addItem(str(text))
        self._items.append(text)

    def remove_item(self, text):
        self._items.remove(text)
        current_row = self._view.currentRow()
        self._view.clear()
        for item in self._items:
            self._view.addItem(str(item))

        current_row = min(current_row, len(self._items) - 1)
        if current_row >= 0:
            self._view.setCurrentRow(current_row)

        self._on_selected_item()

    def select_item_by_index(self, index):
        if index >= len(self._items):
            print("Index overflow : {}".format(len(self._items)))
            return False

        self._view.setCurrentRow(index)
        self._on_selected_item()

        return True

    def get_selected_item(self):
        if self._selected_row == -1:
            return None

        return self._items[self._selected_row]

    def add_items(self, texts: List):
        for t in texts:
            self.add_item(t)

    def set_on_item_selected_listener(self, listener_fn):
        assert callable(listener_fn), "Listener must be Callable Likes Function ..., but {}".format(listener_fn)
        self._listener_fn = listener_fn

    def clear_selected(self):
        self._view.clearSelection()
        self._selected_row = None

    def remove_all_items(self):
        self._items = []
        self._view.clear()

    def get_items(self):
        return copy.deepcopy(self._items)

    def set_enable(self, is_enable):
        self._view.setEnabled(is_enable)

    def filter_like(self, value=str, lambda_fn: Callable = None):
        self._view.clear()
        for item in self._items:
            field = lambda_fn(item)
            if value in field:
                self._view.addItem(str(item))


class TextEditWrapper:
    def __init__(self, view: QTextEdit):
        self._view = view

    def get_text(self):
        return self._view.toPlainText()

    def set_text(self, value):
        print("search {}".format(value))
        try:
            self._view.setText(value)
        except Exception as e:
            print(e)


class ButtonWrapper:
    def __init__(self, btn_view: QPushButton):
        self._btn_view = btn_view
        self._on_clicked_fn = None
        self._btn_view.clicked.connect(self._on_clicked)

    def _on_clicked(self):
        if self._on_clicked_fn is not None:
            self._on_clicked_fn()

    def set_enable(self, is_enable: bool):
        self._btn_view.setDisabled(not is_enable)

    def set_on_clicked_listener(self, listener_fn):
        assert callable(listener_fn), "Listener must be Callable"
        self._on_clicked_fn = listener_fn

    def set_text(self, text):
        self._btn_view.setText(text)


class SpinBoxWrapper:
    def __init__(self, view: QSpinBox):
        self._view = view
