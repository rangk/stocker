from app.views.base_view import LabelWrapper


class MonitorStatusView(LabelWrapper):
    def start_monitoring(self):
        self._label_view.setText("전략 감시중")
        self._label_view.setStyleSheet("color:#0F0;background-color:#FFF")

    def stop_monitoring(self):
        self._label_view.setText("나의 전략들")
        self._label_view.setStyleSheet("color:#000;background-color:#FFF")
